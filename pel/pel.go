package pel

import (
	"crypto/cipher"
	"io"
)

type EncryptReader struct {
	cipher cipher.Stream
	src    io.Reader
}

type EncryptWriter struct {
	cipher cipher.Stream
	dst    io.Writer
}

func NewEncryptReader(src io.Reader, cipher cipher.Stream) *EncryptReader {
	r := new(EncryptReader)
	r.cipher = cipher
	r.src = src
	return r
}

func NewEncryptWriter(dst io.Writer, cipher cipher.Stream) *EncryptWriter {
	w := new(EncryptWriter)
	w.cipher = cipher
	w.dst = dst
	return w
}

func (r *EncryptReader) Read(p []byte) (n int, err error) {
	buf := make([]byte, len(p))
	n, err = r.src.Read(buf)
	if err != nil {
		return
	}
	result := make([]byte, n)
	r.cipher.XORKeyStream(result, buf[:n])
	copy(p, result[:n])
	return n, nil
}

func (w *EncryptWriter) Write(p []byte) (n int, err error) {
	result := make([]byte, len(p))
	w.cipher.XORKeyStream(result, p)
	n, err = w.dst.Write(result)
	return
}
